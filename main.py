from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.graphics import Color, Ellipse, Rectangle, Line
from kivy.core.window import Window

# Widget holding the canvas for graph building
class TuringCanvas(FloatLayout):

    def __init__(self, **kwargs):
        super(TuringCanvas, self).__init__(**kwargs)
        self.state_list = []
        self.state_list.append(StartState())

        self.selected_state = None

        self.redraw()

    # on_touch_down behaves as follows,
    # detects first collision with a state in the state list. If the collision is a double tap select.
    # If an element was already selected, create an transition
    # If no element was previously selected and no collision occured, create new state
    # If no collision occured and a state was selected, deselect state.
    def on_touch_down(self, touch):
        collision = False

        for s in self.state_list:
            if s.collision(touch):
                if touch.is_double_tap:
                    if self.selected_state is not None:
                        self.selected_state.deselect()
                        Transition(self.selected_state, s)
                    self.selected_state = s
                    self.selected_state.select()
                collision = True
                break

        if self.selected_state is None and not collision:
            self.state_list.append(State(center=(touch.x, touch.y), size=(50,50), color=Color(0,1,0)))
        elif not collision:
            self.selected_state.deselect()
            self.selected_state = None
        
        self.redraw()

    # Move the selected state.
    def on_touch_move(self, touch):
        if self.selected_state is not None and self.selected_state.collision(touch):
            self.selected_state.set_center(touch.pos)

        self.redraw()

    def on_touch_up(self, touch):
        pass

    # Clear canvas and call draw on all states.
    def redraw(self):
        self.canvas.clear()
        for s in self.state_list:
            s.draw(self.canvas)

# Holds a state of the turing machine.
class State(object):
    def __init__(self, **kwargs):
        self.transition_list = []

        if "center" in kwargs:
           self.center = kwargs['center']
        else:
            raise(Exception("Center must be defined when State is instantiated:\nE.g. center=(0,0)"))

        if "color" in kwargs:
            self.color = kwargs['color']
            self.standard_color = self.color
        else:
            raise(Exception("Color must be defined when State is instantiated:\nE.g. center=Color(1,1,1)"))

        if "size" in kwargs:
            self.size = kwargs['size']
        else:
            raise(Exception("Size must be defined when State is instantiated:\nE.g. size=(50, 50)"))

    # Is the touch within the ellipse
    def collision(self, touch):
        r = ((self.center[0] - touch.x)/(self.size[0]/2))**2 + ((self.center[1] - touch.y)/(self.size[1]/2))**2

        if r <= 1:
            return True

        return False

    # Change colour of selected state
    def select(self):
        self.color = Color(0, 0, 1)

    # Change colour back
    def deselect(self):
        self.color = self.standard_color

    # Set the central location of the node.
    def set_center(self, center):
        if len(center) != 2:
            raise AttributeError("State position can only take tuple of size 2")
        self.center = center

    def get_center(self):
        return self.center

    # Draw each transition between states.
    def draw(self, canvas):
        for e in self.transition_list:
            e.draw(canvas)
        canvas.add(self.color)
        canvas.add(Ellipse(size=self.size, pos=(self.center[0] - self.size[0]/2, self.center[1] - self.size[1]/2)))

# Connect states with transition
class Transition(object):
    def __init__(self, start, end):
        start.transition_list.append(self)
        self.start = start
        self.end = end

    # Line drawn between states, using bezier with no mid point.
    # TODO: Map bezier anchor point by using line normal with fixed magnitude.
    # TODO: Map line to closest points on surface of states, rather than centers.
    # TODO: Find a way to show directionality of transitions.
    def draw(self, canvas):
        canvas.add(Color(0,0,0))
        canvas.add(Line(bezier=(self.start.get_center()[0], self.start.get_center()[1], self.end.get_center()[0], self.end.get_center()[1])))

# Basic start state.
class StartState(State):
    def __init__(self, **kwargs):
        super(StartState, self).__init__(center=(25, 25), size=(50,50), color=Color(1,0,0))

class TuringApp(App):
    def build(self):

        Window.clearcolor = (1,1,1,1)
        return TuringCanvas()

if __name__ == '__main__':
    app = TuringApp()
    app.run()
